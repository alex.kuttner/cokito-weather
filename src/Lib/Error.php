<?php namespace Cokito\BomWeather;

use Cokito\BomWeather\Config\Paths;

class Error
{
    private static $logPath;

    public function __construct()
    {
        $this->logPath = (new Paths)->logs() . '/';
    }

    public static function log($message)
    {
        if(!file_exists(self::$logPath . date('d-m-Y') . '.txt'))
        {
            fopen(self::$logPath . date('d-m-Y') . '.txt',"w");
        }

        $file = fopen(self::$logPath . date('d-m-Y') . '.txt',"a");
        fwrite($file, "\n" . date('d-m-Y h:m:s') . ' Message: ' . $message);
        fclose($file);
    }
}