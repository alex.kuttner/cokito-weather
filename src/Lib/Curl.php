<?php namespace Cokito\BomWeather;

class Curl {

    /**
     * Download the XML file from the Bureau of Meteorology
     */
    public function downloadFile($remoteFileTime = null)
    {
        set_time_limit(0);
        $fp = fopen ($this->localXmlPath, 'w+'); // TODO:: if unable to open write to the throw exception
        $ch = curl_init($this->remoteXmlPath);
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($ch) ;
        curl_close($ch);
        fclose($fp);

        //Set the modified file time to be the same as the remote file so we can compare the two files in compareTimestamps()
        if($remoteFileTime !== null)
        {
            touch($this->localXmlPath, $remoteFileTime);
        }
        else
        {
            $remoteFileTime = filemtime($this->remoteXmlPath);
            touch($this->localXmlPath, $remoteFileTime);
        }
    }
}