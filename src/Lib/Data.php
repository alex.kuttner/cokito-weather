<?php namespace Cokito\BomWeather;

class Data
{
    private $state;
    private $cacheTime;
    private $localXmlPath;
    private $remoteXmlPath;

    private $counter = 2;

    /**
     * Set the required paramaters for functions in the class
     *
     * Data constructor.
     * @param $state
     */
    public function __construct(string $state)
    {
        $this->state = $state;

        $this->remoteXmlPath = BOMCONFIG['states'][$state];

        // Set how many seconds between checking if a new xml file is available on the bom servers
        $this->cachePath = BOMCONFIG['cache'];
        $this->cacheTime = BOMCONFIG['cacheTime'];

        // Location of locally stored xml file
        $this->localXmlPath = $this->cachePath . '/' . $this->state . '.xml';
    }


    /**
     * Load the state data
     *
     * @return \SimpleXMLElement
     */
    public function load()
    {
        try
        {
            //Check cache folder exists
            if(is_dir($this->cachePath))
            {
                //Check if the local file exists and when last modified
                if(is_file($this->localXmlPath))
                {
                    $stats = stat($this->localXmlPath);

                    $timeLastAccessed = $stats[8] + $this->cacheTime;

                    if (time() > $timeLastAccessed)
                    {
                        //Compare the modified file times and download if needed
                        $this->compareTimestamps();
                    }
                }
                else
                {
                    //File does not exist, download file
                    $this->downloadFile();
                }

                $file = @simplexml_load_file($this->localXmlPath);

                if($file === false && $this->counter > 0)
                {
                    $this->counter--;

                    unlink($this->localXmlPath);
                    $this->load();
                }

                return $file;
            }
            else
            {
                throw new \Exception('Cache folder does not exist at /src/.');
            }
        }
        catch(\Exception $e)
        {
            echo 'Exception thrown! ',  $e->getMessage(), "\n";
        }
    }

    /**
     * Create a json file containing all the state suburbs that have weather available
     */
    public function createSuburbList($data)
    {
        if(!is_file($this->cachePath . '/suburbs-' . $this->state . '.json'))
        {
            $jsonFilePath = $this->cachePath . '/suburbs-' . $this->state.'.json';

            $suburbList = $data->xpath('forecast/area[@description]');

            $suburbArray = [];

            foreach ($suburbList as $item) {
                $suburbArray[] = (string) $item['description'];
            }

            //Sort in alphabetical order
            sort($suburbArray);

            //Create the file and put all the suburbs in there, this will be used later for ajax requests
            file_put_contents($jsonFilePath, json_encode($suburbArray));
        }
    }

    /**
     * Download the XML file from the Bureau of Meteorology
     */
    public function downloadFile($remoteFileTime = null)
    {
        set_time_limit(0);
        $fp = fopen ($this->localXmlPath, 'w+'); // TODO:: if unable to open write to the throw exception
        $ch = curl_init($this->remoteXmlPath);
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($ch) ;
        curl_close($ch);
        fclose($fp);

        //Set the modified file time to be the same as the remote file so we can compare the two files in compareTimestamps()
        if($remoteFileTime !== null)
        {
            touch($this->localXmlPath, $remoteFileTime);
        }
        else
        {
            $remoteFileTime = filemtime($this->remoteXmlPath);
            touch($this->localXmlPath, $remoteFileTime);
        }
    }

    /**
     * Compare the last modified times to determine weather we need to download a new copy or not
     */
    public function compareTimestamps()
    {
        $remoteFileTime = filemtime($this->remoteXmlPath);
        $localFileTime = filemtime($this->localXmlPath);

        if($remoteFileTime !== $localFileTime)
        {
            $this->downloadFile($remoteFileTime);
        }
    }

}
