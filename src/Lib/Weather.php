<?php namespace Cokito\BomWeather;

class Weather
{
    private $data;

    /**
     * Return the state weather using the defined capital city
     * Find all the Max temps available
     *
     * @param $data
     * @param $state
     * @return json
     */
    public function getState($data, $state)
    {
        $this->data = $data;

        // Get state using abbreviaiton, example vic = Victoria
        return $this->data(ucfirst(BOMCONFIG['capitals'][$state]));

    }

    /**
     * Return the suburb
     *
     * Note: Suburb is case sensitive
     *
     * @param $data
     * @param $suburb
     * @return json
     */
    public function getSuburb($data, $suburb)
    {
        $this->data = $data;

        return $this->data($suburb);
    }

    /**
     * @param $captialOrSuburb
     * @return json
     */
    public function data($captialOrSuburb)
    {
        // keeps track of the number of days in the week we are up to
        $x = 0;
        $arrayTemps = [];

        //This will continue on until all the days are retrieved
        while (0 <= $x)
        {
            // 2) Get the data for the current day
            if(empty($this->data->xpath('forecast/area[@description="' . $captialOrSuburb . '"]')))
            {
                // captialOrSuburb not found. (search is case sensitive)
                return 'No data found';
            }
            $currentDayData = $this->data->xpath('forecast/area[@description="' . $captialOrSuburb . '"]')[0]->{'forecast-period'}[$x];

            if($currentDayData)
            {
                // 3) Holds the max temperature for today
                $todayMaxTemp = $currentDayData->xpath("./element[@type='air_temperature_maximum']");

                // TODO:: this item code is not always present see QLD
                $iconCode = $currentDayData->xpath("./element[@type='forecast_icon_code']");

                /**
                 * Extract the date attribute "start-time-local" from the node "forecast-period"
                 * Get the time stamp and then extract the date
                 */
                $timestamp = (string) $currentDayData['start-time-local'];
                $attributeDate = substr($timestamp, 0, 10);


                // 4) Create Mon, Tues, Wed, etc. Based on the XML date.
                //We can use them to associate the max temp like "Mon" => "40" .... HOT :p
                $dt = strtotime($attributeDate); //Convert to seconds
                $dayTxt = strtolower(date("D", $dt));

                // 5) Add the maxTemp to the $arrayTemps
                // At the end of the day 5pm I think they remove today's temperature so $todayMaxTemp[0] will be undefined so skip
                if(isset($todayMaxTemp[0]))
                {
                    $arrayTemps[$dayTxt] = [
                        'suburb' => $captialOrSuburb,
                        'maxTemp' => (string) $todayMaxTemp[0],
                        'iconCode' => (string) $iconCode[0]
                    ];
                }

                $x++;
            }
            else
            {
                //No more days left
                $x = -1;
            }
        }

        return json_encode($arrayTemps);
    }
}
