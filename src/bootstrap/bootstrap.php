<?php

if(defined('DISPLAY_ERRORS_ENABLED'))
{
    ini_set('display_errors', 1);
    error_reporting(E_ALL);
}

$bomConfig = include(dirname(__DIR__).'/config/config.php');

// If another config file has been added then merge and replace any that already exist
if(defined('CUSTOM_CONFIG_PATH'))
{
    $overide = include(CUSTOM_CONFIG_PATH);

    foreach($overide as $key => $value)
    {
        // Check if the value is an array, if so then loop it
        if(is_array($value))
        {
            foreach($value as $key2 => $value2)
            {
                $bomConfig[$key][$key2] = $value2;
            }
        }
        else
        {
            $bomConfig[$key] = $value;
        }
    }
}

define('BOMCONFIG', $bomConfig);

require dirname(dirname(__DIR__)) . '/vendor/autoload.php';
