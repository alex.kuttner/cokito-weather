<?php

return [
    'states' => [
        'vic' => 'ftp://ftp2.bom.gov.au/anon/gen/fwo/IDV10753.xml',
        'nsw' => 'ftp://ftp2.bom.gov.au/anon/gen/fwo/IDN11060.xml', // Same file as act
        'act' => 'ftp://ftp2.bom.gov.au/anon/gen/fwo/IDN11060.xml', // Same file as nsw
        'sa'  => 'ftp://ftp2.bom.gov.au/anon/gen/fwo/IDS10044.xml',
        'wa'  => 'ftp://ftp2.bom.gov.au/anon/gen/fwo/IDW14199.xml',
        'tas' => 'ftp://ftp2.bom.gov.au/anon/gen/fwo/IDT16710.xml',
        'qld' => 'ftp://ftp2.bom.gov.au/anon/gen/fwo/IDQ10095.xml',
        'nt'  => 'ftp://ftp2.bom.gov.au/anon/gen/fwo/IDD10207.xml',
    ],

    'capitals' => [
        'vic' => 'Melbourne',
        'nsw' => 'Sydney',
        'qld' => 'Brisbane',
        'sa'  => 'Adelaide',
        'wa'  => 'Perth',
        'tas' => 'Tasmania',
        'nt'  => 'Darwin',
        'act' => 'Canberra'
    ],

    'cache' => dirname(dirname(__FILE__)) . '/cache',
    'cacheTime' => 1800, // 60 sec * 30 = 30 Mins = 1800 sec
];