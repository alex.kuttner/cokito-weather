<?php
/**
 * Ajax Request Handler
 *
 * This will handle any ajax request for weather information
 * All requests must be GET requests
 *
 * $_GET['state']      = State abbreviation such as "vic" not "Victoria"
 * $_GET['suburb']     = Suburb. This is case sensitive. It must match the suburb in the suburb list json file.
 *
 * $_GET['suburbList'] = State abbreviation such as "vic" not "Victoria". Get the suburb list for the state.
 */
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && isset($_GET['state']))
{
    require '../../src/bootstrap/bootstrap.php';

    $weather = new Cokito\BomWeather\Facades\WeatherFacade();

    if(isset($_GET['state']) && !isset($_GET['suburb']))
    {
        $maxTemps = $weather->captial($_GET['state']);
    }
    elseif(isset($_GET['state']) && isset($_GET['suburb']))
    {
        $maxTemps = $weather->suburb($_GET['state'], $_GET['suburb']);
    }
    else
    {
        $maxTemps = [];
    }

    echo $maxTemps;
}
elseif(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && $_GET['suburbList'])
{
    require '../../src/bootstrap/bootstrap.php';

    $weather = new Cokito\BomWeather\Facades\WeatherFacade();

    echo $weather->suburbList($_GET['suburbList']);
}
else
{
    echo 'The Ajax Handler failed';
}