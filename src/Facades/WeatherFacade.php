<?php namespace Cokito\BomWeather\Facades;

use Cokito\BomWeather\Data;
use Cokito\BomWeather\Weather;

class WeatherFacade
{
    private $weather;
    
    public function __construct()
    {
        $this->weather = new Weather;
    }

    /**
     * Will return the major city depending on state
     *
     * @param $state
     * @return \Cokito\BomWeather\json
     */
    public function captial($state)
    {
        $data = new Data($state);
        $simpleXml = $data->load(); // Load the required data
        $data->createSuburbList($simpleXml); // Create suburb list

        return $this->weather->getState($simpleXml, $state);
    }

    /**
     * Return the suburb
     *
     * Note: Suburb is case sensitive
     *
     * @param $state
     * @param $suburb
     * @return \Cokito\BomWeather\json
     */
    public function suburb($state, $suburb)
    {
        $data = new Data($state);
        $simpleXml = $data->load(); // Load the required data
        $data->createSuburbList($simpleXml); // Create suburb list

        return $this->weather->getSuburb($simpleXml, $suburb);
    }

    /**
     * Return the json file that has been created
     *
     * @param $state
     * @return bool|json file
     */
    public function suburbList($state)
    {
        return file_get_contents(BOMCONFIG['cache'] . '/suburbs-' . $state . '.json');
    }
}
