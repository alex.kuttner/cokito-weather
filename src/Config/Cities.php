<?php namespace Cokito\BomWeather\Config;

class Cities
{
    public static function capitals($state)
    {
        $data = ['vic' => 'Melbourne',
            'nsw' => 'Sydney',
            'qld' => 'Brisbane',
            'sa'  => 'Adelaide',
            'wa'  => 'Perth',
            'tas' => 'Tasmania',
            'nt'  => 'Darwin',
            'act' => 'Canberra'];

        return ( isset($data[$state]) ) ? $data[$state] : null;
    }
}