<?php namespace Cokito\BomWeather\Config;

class Paths
{
    public static function cache()
    {
        return dirname(dirname(__FILE__)) . '/cache';
    }

    public static function logs()
    {
        return dirname(dirname(__FILE__)) . '/logs';
    }
}