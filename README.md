Bureau of Meteorology (BOM)
================

- [Installation](#installation)
- [Configuration](#configuration)
- [Usage](#usage)
- [Testing](#testing)
- [Contributing](#contributing)
- [License](#license)

Installation
-----
To install just clone the master branch into your project and run composer install

Example
```bash
git clone git@gitlab.com:cokito/cokito-weather.git
cd cokito-weather
composer install
```


Configuration
-----
The configuration is stored in `/src/config/config.php` if you would like to update this do not update this file, simply add a custom config file.    
For example a common thing to do is change the directory where the xml and json files are cached (default is /src/cache).  
Just define CUSTOM_CONFIG_PATH before you require bootstrap.php file like so  
```php
define('CUSTOM_CONFIG_PATH', '../../custom/config.php');

require '../../src/bootstrap/bootstrap.php';
```
In the your custom config.php file simple add your config you would like to overwrite
```php
<?php
return [
    'cache' => '/var/www/domain.com.au/storage/bom/cache'
];
```
It goes without saying the folder needs to be writable.


Usage
-----

See code for more info on how to implement into your project


Testing
-------
Run the phpunit in your terminal

``` bash
$ phpunit
```


Contributing
------------

Please see [CONTRIBUTING](https://gitlab.com/cokito/cokito-weather/blob/master/CONTRIBUTING.md) for details.


License
-------

The MIT License (MIT). Please see [License File](https://gitlab.com/cokito/cokito-weather/blob/master/LICENSE) for more information.
