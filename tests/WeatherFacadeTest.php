<?php

use Cokito\BomWeather\Facades\WeatherFacade;
use Cokito\BomWeather\Data;

class WeatherFacadeTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Test Json response is returned
     */
    public function testCapitalReturnsJson()
    {
        // We must define the config file here. Once defined here it will be accesible to all methods in this class
        $bomConfig = include('src/config/config.php');
        define('BOMCONFIG', $bomConfig);

        $myObj = new WeatherFacade();

        $json = $myObj->captial('nsw');
        $this->assertJson($json);
    }

    /**
     * Test Json response is returned
     */
    public function testSuburbReturnsJson()
    {
        $myObj = new WeatherFacade();

        $json = $myObj->suburb('nsw', 'Penrith');
        $this->assertJson($json);
    }

    /**
     * Test the creation of the suburb list file
     */
    public function testCreateSuburbList()
    {
        $data = new Data('nsw');
        $simpleXml = $data->load(); // Load the required data
        $data->createSuburbList($simpleXml); // Create suburb list

        $this->assertFileExists(BOMCONFIG['cache'] . '/suburbs-nsw.json');
    }
}
