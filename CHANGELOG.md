# Change Log

All notable changes to this publication will be documented in this file.

## 1.0.0 - 19-11-2017

First stable release.
