$(document).ready(function() {

    /* The call for State max temperatures
     -------------------------------------------------------------------------------*/
    $('#states').change(function(event) {

        //Show loading icon
        $(".preloader").fadeIn("slow");

        event.preventDefault();

        //Set the path where the request.php file is and the images are
        var urlPath = '../../src/Handlers/AjaxHandler.php';
        var imgPath = 'img/icons/';

        //This is the drop down list used in the example
        var stateAbbrev = $('#states option:selected').val();

        $.ajax({
            dataType: 'json',
            type: 'GET',
            url: urlPath,
            data:{
                'state': stateAbbrev
            }
        }).done(function(response) {

            $('#display-weather').empty();

            $.each(response, function(index, value) {
                $('#display-weather').append('<li><img src="' +imgPath+getImage(value.iconCode)+ '"> ' +capitalizeFirstLetter(index)+ ': ' +value.maxTemp+ '</li>');
            });

            //Fill the second list with the states suburbs
            getSuburbList(stateAbbrev);

            //Hide loading icon
            $(".preloader").fadeOut("slow");

        }).fail(function(xhr, status, error) {
            console.error(xhr.responseText)
            alert('BOM failed, check logs for more information.');
        });

    });

    /* The call to get suburb specific weather
     -------------------------------------------------------------------------------*/
    $('#suburbs').change(function(event) {

        //Show loading icon
        $(".preloader").fadeIn("slow");

        event.preventDefault();

        //Set the path where the request.php file is and the images are
        var urlPath = '../../src/Handlers/AjaxHandler.php';
        var imgPath = 'img/icons/';

        //Get the previously selected state as we need this to get the suburbs weather
        var selectedState = $('#states option:selected').val();

        //This is the drop down list used in the example
        var selectedOption = $('#suburbs option:selected').val();

        $.ajax({
            dataType: 'json',
            type: 'GET',
            url: urlPath,
            data:{
                'state':selectedState,
                'suburb':selectedOption
            }
        }).done(function(response) {

            $('#display-weather').empty();

            $.each(response, function(index, value){
                $('#display-weather').append('<li><img src="' +imgPath+getImage(value.iconCode)+ '"> ' +capitalizeFirstLetter(index)+ ': ' +value.maxTemp+ '</li>');
            });

            //Hide loading icon
            $(".preloader").fadeOut("slow");

        }).fail(function(xhr, status, error) {
            console.error(xhr.responseText)
            alert('BOM failed, check logs for more information.')

        });

    });

    /* The call to get the list of suburbs to populate the drop down list
     -------------------------------------------------------------------------------*/
    function getSuburbList(stateAbbrev)
    {
        $('#suburbs').empty();

        var urlPath = '../../src/Handlers/AjaxHandler.php';

        $.ajax({
            dataType: 'json',
            type: 'GET',
            url: urlPath,
            data:{
                'suburbList':stateAbbrev
            }
        }).done(function(response) {
            $.each(response, function(i, value) {
                $('#suburbs').append($('<option>').text(value).attr('value', value));
            })

        }).error(function (xhr, status, error) {
            console.error(xhr.responseText)
        })
    }

    /* Simple function the get the correct name of the icon
     -------------------------------------------------------------------------------*/
    function getImage (iconCode)
    {
        if(iconCode == '1')
        {
            return 'sunny.png'
        }
        if(iconCode == '2')
        {
            return 'clear.png'
        }
        if(iconCode == '3')
        {
            return 'partly-cloudy.png'
        }
        if(iconCode == '4')
        {
            return 'cloudy.png'
        }
        if(iconCode == '5')
        {
            return ''
        }
        if(iconCode == '6')
        {
            return 'haze.png'
        }
        if(iconCode == '7')
        {
            return ''
        }
        if(iconCode == '8')
        {
            return 'light-rain.png'
        }
        if(iconCode == '9')
        {
            return 'wind.png'
        }
        if(iconCode == '10')
        {
            return 'fog.png'
        }
        if(iconCode == '11')
        {
            return 'showers.png'
        }
        if(iconCode == '12')
        {
            return 'rain.png'
        }
        if(iconCode == '13')
        {
            return 'dusty.png'
        }
        if(iconCode == '14')
        {
            return 'frost.png'
        }
        if(iconCode == '15')
        {
            return 'snow.png'
        }
        if(iconCode == '16')
        {
            return 'storm.png'
        }
        if(iconCode == '17')
        {
            return 'light-showers.png'
        }
    };

    /* Function to capitalise first character for strings
     -------------------------------------------------------------------------------*/
    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

});

