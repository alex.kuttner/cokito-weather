<?php
// define('DISPLAY_ERRORS_ENABLED', true);
// define('CUSTOM_CONFIG_PATH', '../../custom/config.php');

require '../../src/bootstrap/bootstrap.php';

$weather = new Cokito\BomWeather\Facades\WeatherFacade();

$x = 0;

foreach (json_decode($weather->captial('nsw')) as $day => $obj) {

    $suburb = $obj->suburb;

    if($x === 0)
    {
        echo ucfirst($suburb) . '<hr>';
        $x++;
    }

    echo ucfirst($day) . ': ' .$obj->maxTemp. ' - Icon: ' .$obj->iconCode. '<br>';
}